import { CHANGE_CURRENCY } from './actionTypes';

export const changeCurrency = (selectedCurrency) => ({
    type: CHANGE_CURRENCY, 
    payload: {
        selectedCurrency: selectedCurrency
    }
})