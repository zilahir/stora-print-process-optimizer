import { SET_RESULT } from './actionTypes';

export const setResult = (calculationResult) => ({
    type: SET_RESULT, 
    payload: {
        calculationResult: calculationResult
    }
})