import { CHART_LINE } from './actionTypes';

export const setChartLine = (chartLineArray) => ({
    type: CHART_LINE, 
    payload: {
        chartValues: chartLineArray
    }
})