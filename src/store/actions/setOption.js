import { SET_OPTION } from './actionTypes';

export const setOption = (selectedOption, selectedValue) => ({
    type: SET_OPTION, 
    payload: {
        selectedOption: selectedOption,
        selectedValue: selectedValue
    }
})