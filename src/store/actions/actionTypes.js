export const TEST_ACTION_TYPE = 'TEST_ACTION_TYPE';
export const SET_OPTION = 'SET_OPTION';
export const SET_RESULT = 'SET_RESULT';
export const CHART_LINE = 'CHART_LINE';
export const CHANGE_CURRENCY = 'CHANGE_CURRENCY';