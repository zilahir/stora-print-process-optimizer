import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import TestReducer from './reducers/Test';
import setOpions from './reducers/setOption';
import setResult from './reducers/setResult';
import chartLine from './reducers/chartLine';
import changeCurrency from './reducers/changeCurrency';

const rootReducer = combineReducers({
    test: TestReducer,
    selectedOptions: setOpions,
    calculationResult: setResult,
    chartValues: chartLine,
    chosenCurrency: changeCurrency
});

let composeEnhancers = compose;

const configureStore = () => {
  return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
};

export default configureStore;