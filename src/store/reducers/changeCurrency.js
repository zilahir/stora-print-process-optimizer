import { CHANGE_CURRENCY } from './../actions/actionTypes';

const initialState = {
    chosenCurrency: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_CURRENCY: {
            return {
                ...state, 
                selectedCurrency: action.payload.selectedCurrency
            }
        }
        default:
            return state;
    }
};

export default reducer;