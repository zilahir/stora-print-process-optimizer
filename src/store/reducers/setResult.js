import { SET_RESULT } from './../actions/actionTypes';

const initalState = {
    calculationResult: null
}

const reducer = (state = initalState, action) => {
    switch (action.type) {
        case SET_RESULT:
            return {
                ...state,
                calculationResult: action.payload.calculationResult
            };
        default: 
            return state;
    }
}

export default reducer;