import { CHART_LINE } from './../actions/actionTypes';

const initialState = {
    chartValues: []    
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case CHART_LINE: 
            return {
                ...state, 
                chartValues: action.payload.chartValues
            }
        default:
            return state;
    }
};

export default reducer;