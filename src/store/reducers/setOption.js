import {
    SET_OPTION
} from './../actions/actionTypes';

const initialState = {
    selectedOptions: {}
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_OPTION:
    return {
        ...state,
        selectedOptions: {
            ...state.selectedOptions, 
            [action.payload.selectedOption]: action.payload.selectedValue
        }
      };    
    default:
      return state;
  }
};

export default reducer;