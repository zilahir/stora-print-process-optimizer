

/**
 * Created by User on 10/19/2017.
 */
import RuleJS from 'ruleJS'
import XLSX from 'xlsx';

export default class Calculator{

  constructor(excel_url, selectedSheet){
      this.excel_url = excel_url;
      this.selectedSheet = selectedSheet;
  }
  /*global XLSX */
  X = XLSX;

  global_wb;

  excel_url = '';

  rules = RuleJS.ruleJS;
  HTMLOUT = document.createElement('div');

  writers = {};

  observers = {};

  selectedSheet = "";

  process_wb = function(wb) {
    var _self = this;

    var to_html = function to_html(workbook) {
      _self.HTMLOUT.innerHTML = "";
      workbook.SheetNames.forEach(function(sheetName) {
          if(sheetName == _self.selectedSheet){
              var htmlstr = _self.X.write(workbook, {sheet:sheetName, type:'binary', bookType:'html'});
              _self.HTMLOUT.innerHTML += htmlstr;
          }
      });
      //_self.rules = RuleJS.ruleJS(_self.HTMLOUT);
        console.debug("ruleJS", RuleJS)
      _self.rules.init(_self.HTMLOUT);

      _self.X.createDependencies();

      /*var item;
      for (item in _self.X.eRows){
        var row = _self.X.eRows[item];
        for (var a=0;a<row.cells.length;a++) {
          var cell = row.cells[a];
          if (cell.dependencies.length > 0){
            for (var i=0;i<cell.dependencies.length;i++){
              var celln = _self.X.getCellByReference(cell.dependencies[i]);
              _self.prepareDependencies(celln);
            }
          }
        }
      }*/

      //console.debug("array",_self.X.eRows)
      //console.debug("formulas",X.formulas)
        console.debug("tables",_self.X.tables['flutingTypes'])
    }

    this.global_wb = wb;
    to_html(wb);
  }

  calculationStart = function(writers,observers,callback) {
      var _self = this;
      this.writers = writers;
      this.observers = observers;
      document.body.appendChild(this.HTMLOUT);

      this.HTMLOUT.setAttribute('id','htmlout')
      this.HTMLOUT.setAttribute('style','visibility: visible; display: none')

      var oReq = new XMLHttpRequest();
      oReq.open("GET", this.excel_url, true);
      oReq.responseType = "arraybuffer";

      oReq.onload = function(e) {
        var arraybuffer = oReq.response;

        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");

        /* Call XLSX */
        _self.process_wb(_self.X.read(bstr, {type: "binary"}));
        typeof callback === 'function' && callback(_self.X);
      }
      oReq.send();
  }

  executeWriters = function(){
    var item;
    for (item in this.writers) {
      var row = this.writers[item];
      console.debug("row",row);      
      this.X.eRows[item].cells[0].value = row;
      document.querySelector('#'+this.X.eRows[item].cells[0].ref).setAttribute("value",row);
    }
  }

  executeObservers = function(){
    var item;
    for (item in this.observers) {
        if (this.X.eRows[item].cells.length == 1){
            this.observers[item] = this.X.eRows[item].cells[0].value;
        }
        else {
            this.observers[item] = [];
            for (var i=0;i<this.X.eRows[item].cells.length;i++){
                try {
                    this.observers[item].push(this.X.eRows[item].cells[i].value);
                }
                catch(error){
                    console.error("observer error", error)
                }
            }
        }
    }
  }

  startCalculationProcess = function(){
    var item;
    for (item in this.X.eRows){
      var row = this.X.eRows[item];
      for (var a=0;a<row.cells.length;a++) {
        var cell = row.cells[a];
        if (cell.formula != null) {
          this.calculateAll(cell);
        }
      }
    }
    //this.calculateDependencies();
    console.debug("array after",this.X.eRows)
    this.executeObservers();
  }

  calculateAll = function(cell){
    var parsed = this.rules.parse(cell.formula, document.querySelector("#"+cell.ref));

    if (parsed.result !== null) {
      cell.value = parsed.result;
      document.querySelector("#"+cell.ref).setAttribute("value",parsed.result);
      console.debug("formula", );
    }
    if (parsed.error) {      
      console.error("calculateAll ERROR",parsed.error);
    }
    /*
    if (cell.dependencies.length > 0){
      for (var i=0;i<cell.dependencies.length;i++){
        var celln = this.X.getCellByReference(cell.dependencies[i]);
        this.prepareDependencies(celln);
      }
    }
    */
  }

  prepareDependencies = function(cell){
    if (cell != undefined){
      var index = this.dependencyArray.indexOf(cell.ref);
      if (index > -1){
        this.dependencyArray.splice(index, 1);
      }
      this.dependencyArray.push(cell.ref);
      if (cell.dependencies.length > 0) {
        for (var i = 0; i < cell.dependencies.length; i++) {
          this.prepareDependencies(this.X.getCellByReference(cell.dependencies[i]));
        }
      }
    }
  }

  dependencyArray = [];

  calculateDependencies = function(){
    for (var i=0;i<this.dependencyArray.length;i++){
      var cell = this.X.getCellByReference(this.dependencyArray[i]);
      if (cell != undefined){
        var parsed = this.rules.parse(cell.formula, document.querySelector("#"+cell.ref));
        if (parsed.result !== null) {
          cell.value = parsed.result;
          document.querySelector("#"+cell.ref).setAttribute("value",parsed.result);
        }
        if (parsed.error) {
          console.error("calculateDependencies ERROR",parsed.error);
        }
      }
    }
  }

  execute = function(){
    this.executeWriters();
    this.startCalculationProcess();
  }

}

