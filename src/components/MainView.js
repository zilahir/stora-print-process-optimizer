import React, { Component } from 'react';
import styled from 'styled-components';
import backgroundImage from './assets/backgroundImage.png';
import TopHeader from './common/TopHeader';
import { Container, Row, Col } from 'react-flexybox';
import Option from './common/Option';
import { options } from './../options';
import Savings from './Savings';
import Chart from './Chart';


const CalculatorContainer = styled.div`    
    display: flex;
    align-self: center;
    justify-content: center;
    align-items: center;
    margin: 100px auto;
    flex-direction: column;
    width: calc(100% - 200px);
`;

const Header = styled.h1`
    margin: 0;
    padding: 0;
    color: #01a2de;
    font-size: 30px;
    span {
        color: #000;
        margin-left: 10px;
    }
`;

const SubHeader = styled.p`
    font-size: 16px;
    margin: 0;
    padding: 0;
    text-align: left;
    color: #000;
    font-weight: normal;
`;

const InputContainer = styled.div`
    padding: 50px;    
    display: flex;
    justify-content: space-between;
    width: 100%;
    align-items: center;
    span {
        color: #02a2de;     
    }
`;

const SectionTitle = styled.h1`
    font-size: 42px;
`;

class MainView extends Component {
    constructor(props) {
        super(props);

        this.state = {            
            demo: null            
        }         
    }    

    render() {                     
        return (
            <div>
                <TopHeader />                    
                <Container fluid>
                <CalculatorContainer style={{backgroundImage: `url(${backgroundImage})`}}>
                    <Row>
                        <Col flex={3} xs={12}>
                            <SectionTitle>
                                Select Options
                            </SectionTitle>
                            <Option color="#ffc929" input="pagination" title="pagination" options={options.pagination} />
                            <Option color="#0098d9" input="reels" title="reelsperday" options={options.reelsperday} />
                            <Option color="#6db42d" input="breaksBefore" title="Web breaks % before" options={options.webbreaksbefore} />
                            <Option color="#6db42d" input="breaksAfter" title="Web break % after" options={options.webbreaksafter} />
                        </Col>
                        <Col flex={6} xs={12}>
                            <Chart />
                        </Col>
                        <Col flex={3} xs={12}>
                            <Savings />
                        </Col>
                    </Row>
                </CalculatorContainer>                     
                </Container>
            </div>                       
        )
    }
}

export default MainView;