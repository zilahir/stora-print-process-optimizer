import React, { Component } from 'react';
import styled from 'styled-components';
import Image from './assets/image.png';
import backgroundImage from './assets/backgroundImage.png';
import Button from './common/Button';
import { connect } from 'react-redux';
import { changeCurrency } from './../store/actions/changeCurrency';

const WelcomeContainer = styled.div`
    width: 900px;
    height: 900px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0 auto;
    div {
        &:first-of-type {
            margin-right: 50px;
        }
    }
`;

const WelcomeImage = styled.img``

const WelcomeImageWrapper = styled.div``;

const WelcomeTextWrapper = styled.div`
    p {
        font-size: 18px;
    }
`;

const CurrencyContainer = styled.div`
    display: flex;
    justify-content: sapce-around;    
    position: relative;
    h1 {
        font-size: 18px;
        position: absolute;
        top: 0;
        width: 100%;
    }
`;

const WelcomeText = styled.h1`
    color: #01a2de;
    font-size: 30px;
`;

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.handleEnter = this.handleEnter.bind(this);
        this.handleExchangeRate = this.handleExchangeRate.bind(this);
        this.state = {
            exchangeRate: "1.0",
            isEur: true, 
            isGpb: false,
        }
        this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
    }

    handleEnter () {
        this.props.history.push('calculator')
    }

    handleExchangeRate(e) {        
        this.setState({
            exchangeRate: e.target.value
        })
    }

    componentDidMount() {
        this.props.onChangeCurrency("EUR");
    }

    handleCurrencyChange (thisCurrency) {        
        this.props.onChangeCurrency(thisCurrency);
        if (thisCurrency === 'EUR') {
            this.setState({
                isEur: true,
                isGpb: false
            })
        } else {
            this.setState({
                isGpb: true,
                isEur: false
            })
        }
    }

    render() {        
        return (
            <WelcomeContainer
                style={{backgroundImage: `url(${backgroundImage})`}}
            >
                <WelcomeImageWrapper>
                    <WelcomeImage src={Image} />
                </WelcomeImageWrapper>
                <WelcomeTextWrapper >
                    <WelcomeText>
                        Print Process Optimizer
                    </WelcomeText>
                    <p>
                        Improve efficiency and productivity. 
                    </p>
                    <p>
                        Save time and money. 
                    </p>
                    <p>
                        Minimize the impact of unplanned stops and increase productivity. 
                        <br/>
                        Print Process Optimizer estimates the savings resulting from increased productivity. 
                        <br/>
                        Start by selecting the currency and the exchange rate. 
                    </p>
                    <div style={{display: `flex`, flexDirection: `row`}}>
                    <CurrencyContainer>
                        <h1>Select Currency</h1>
                    <Button onClick={() => this.handleCurrencyChange('EUR')} isActive={this.state.isEur} color="blue" btnText="EUR" />
                    <Button onClick={() => this.handleCurrencyChange('GBP')} isActive={this.state.isGpb} color="blue" btnText="GBP" />
                    </CurrencyContainer>
                    <CurrencyContainer style={{width: `200px`}}>
                        <h1>Exchange rate</h1>                    
                        <input value={this.state.exchangeRate} onChange={this.handleExchangeRate} type="text" style={{fontWeight: `bold`, color: `#0098d8`, fontSize: `16px`, height: `37px`, position: `absolute`, bottom: `21px`, border: `1px solid #0098d8`, outline: `none`}} />
                    </CurrencyContainer>
                    </div>
                    <Button onClick={(e) => this.handleEnter(e)} btnText="Calculate your saving" />
                </WelcomeTextWrapper>
            </WelcomeContainer>
        )
    }
}

const mapStateToProps = state => ({
    chosenCurrency: state.chosenCurrency.selectedCurrency
})

const mapDispatchToProps = dispatch => ({
    onChangeCurrency: (chosenCurrency) => dispatch(changeCurrency(chosenCurrency))
});

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);