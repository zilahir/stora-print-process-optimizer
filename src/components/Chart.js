import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';
import { connect } from 'react-redux';
import ReactJson from 'react-json-view'
import { data } from './common/initialChartData';

class Chart extends Component {
    constructor(props) {
        super(props);
            
        this.state = {
            data: data
        }
    }

    render() {
        let newData = null;
        if (this.props.calculationResult.calculationResult) {
            let resultArray = []
            for (let i = 0; i<=12; i++) {
                let sum = this.props.calculationResult.calculationResult.savingsYear
                let divider = sum/12;
                let currentNumber = i*divider;
                resultArray.push(currentNumber);
            }

            newData = {
                labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                datasets: [
                {
                    label: 'Saving',        
                    data: resultArray,
                    fill: false,
                    lineTension: 0.3,
                    backgroundColor: 'transparent',
                    borderColor: '#4079ba',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#4079ba',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#4079ba',
                    pointHoverBorderColor: '#4079ba',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    steppedLine: false
                }
                ]
            };
            
        } else {
            newData = {
                labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                datasets: [
                {
                    label: '',        
                    data: [],                    
                }
                ]
            };
        }
        return(
            <div>
                <Line data={newData} />                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    selectedOptions: state.selectedOptions,
    calculationResult: state.calculationResult
})

export default connect(mapStateToProps, null)(Chart);