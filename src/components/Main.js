import React, { Component } from 'react';
import {
    HashRouter,
    Route,
    Switch
  } from "react-router-dom";
import SplashScreen from './SplashScreen';
import MainView from './MainView';
import Costs from './Costs';


class Main extends Component {
    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route exact path="/" component={SplashScreen} />
                    <Route exact path="/calculator" component={MainView} />
                    <Route exact path="/costs" component={Costs} />
                </Switch>
            </HashRouter>
        )
    }
}

export default Main;