import React, { Component } from 'react';
import Option from './common/Option';
import { options } from './../options';
import TopHeader from './common/TopHeader';
import { Container, Row, Col } from 'react-flexybox';
import backgroundImage from './assets/backgroundImage.png';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Line, Chart } from 'react-chartjs-2';
import Button from './common/Button';
import ReactPDF from '@react-pdf/renderer';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

const CalculatorContainer = styled.div`    
    display: flex;
    align-self: center;
    justify-content: center;
    align-items: center;
    margin: 100px auto;
    flex-direction: column;
    width: calc(100% - 200px);
`;

const SectionTitle = styled.h1`
    font-size: 28px;
`;

const SavingContainer = styled.div`
    padding: 0 40px;
`;

const SavingTitle = styled.h1`
    font-size: 42px;
`;

const SavingDetails = styled.div`
    h2 {
        font-size: 20px;
    }
    p {
        font-size: 16px;
    }
`;


const chartOptions = {
    tooltips: {
        intersect: true,
        enabled: false,
        filter: (TooltipItem, index) => {
            console.log(TooltipItem)
            return true
        }
    },    
    elements: {
        point: {
            radius: 0
        }
    }
    
}

class Costs extends Component {
    constructor(props) {
        super(props);
        this.handleSave = this.handleSave.bind(this);
        this.printDocument = this.printDocument.bind(this);
        this.state = {
            isSelectorVisible: true,
            flexDirection: null,
            alignItems: null
        }
    }

    printDocument() {
        const input = document.getElementById('mainContainer');
        html2canvas(input)
          .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF();
            pdf.addImage(imgData, 'JPEG', -90, 20);            
            pdf.save("download.pdf");
            this.setState({
                isSelectorVisible: true, 
                flexDirection: null,
                alignItems: null
            })
          })
        ;
      }
    

    handleSave() {
        this.setState({
            isSelectorVisible: false,
            flexDirection: 'column',
            alignItems: 'center'
        })
        this.printDocument();
    }

    componentDidMount() {
        Chart.pluginService.register({
            beforeRender: function (chart, easing) {
                console.log("chart", chart)
            }
        });
    }
    
    render() {
        let newData = null;     
        let isSelectorVisible = this.state.isSelectorVisible ? '' : 'none';   
        let flexDirection = this.state.flexDirection ? this.state.flexDirection : '';
        let alignItems = this.state.alignItems ? this.state.alignItems : '';
        if (this.props.calculationResult.calculationResult) {
            let resultArray = []
            let resultArray2 = []        
            for (let i = 0; i<=12; i++) {
                let sum = this.props.calculationResult.calculationResult.savingsYear
                let divider = sum/12;
                let currentNumber = i*divider;
                resultArray.push(currentNumber);                                  
                if (this.props.selectedOptions.selectedOptions.weeksMonitoring) {
                    resultArray2.push(this.props.calculationResult.calculationResult.costLong);                    
                }                
            }

            newData = {
                labels: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'],
                datasets: [
                {
                    label: 'Saving',        
                    data: resultArray,
                    fill: false,
                    lineTension: 0.3,
                    backgroundColor: 'transparent',
                    borderColor: '#4079ba',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#4079ba',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#4079ba',
                    pointHoverBorderColor: '#4079ba',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    steppedLine: false,                    
                }, {
                    label: 'Investment',        
                    data: resultArray2,
                    fill: false,
                    lineTension: 0.3,
                    backgroundColor: 'transparent',
                    borderColor: '#1C3142',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#1C3142',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#1C3142',
                    pointHoverBorderColor: '#1C3142',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    steppedLine: false,                     
                }
                ]
            };
            
        } else {
            newData = {
                labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                datasets: [
                {
                    label: '',        
                    data: [],                    
                }
                ]
            };
        }
        return (
                <div>
                    <TopHeader />                    
                    <Container fluid>
                    <CalculatorContainer id="mainContainer" style={{backgroundImage: `url(${backgroundImage})`}}>
                        <Row style={{flexDirection: flexDirection, alignItems: alignItems}} >
                            <Col style={{display: `${isSelectorVisible}`}} flex={3} xs={12}>
                                <SectionTitle>
                                    Cost of the service
                                </SectionTitle>                                
                                    <Option color="#ffc929" input="weeksMonitoring" title="Weeks monitoring" options={options.weeksmonitoring} />
                            </Col>
                            <Col flex={6} xs={12}>
                                <Line options={chartOptions} data={newData} />
                            </Col>
                            <Col flex={3} xs={12}>
                            <SavingContainer>
                                <SavingTitle>
                                    Costs
                                </SavingTitle>
                                <SavingDetails>
                                    <h2>
                                        Investment
                                    </h2>
                                    <p>
                                        {
                                            this.props.calculationResult.calculationResult ? `${this.props.calculationResult.calculationResult.costLong} ${this.props.chosenCurrency}` : `0 ${this.props.chosenCurrency}`
                                        }
                                    </p>
                                    <h2>
                                        Return of investment
                                    </h2>
                                    <p>
                                        {
                                            this.props.calculationResult.calculationResult ? `${parseFloat(this.props.calculationResult.calculationResult.bepMonthLongCalculation).toFixed(2)} Months` : `0 Months`
                                        }
                                    </p>    
                                    <div style={{display: `${isSelectorVisible}`}}>
                                        <Button onClick={this.handleSave} btnText="Save as Pdf" />
                                    </div>                                    
                                </SavingDetails>
                            </SavingContainer>
                            </Col>
                        </Row>
                    </CalculatorContainer>                     
                    </Container>
                </div>
        )
    }
}

const mapStateToProps = state => ({    
    calculationResult: state.calculationResult,
    chosenCurrency: state.chosenCurrency.selectedCurrency,
    selectedOptions: state.selectedOptions,
})

export default withRouter(connect(mapStateToProps, null)(Costs));