import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

const SavingContainer = styled.div`
    padding: 0 40px;
`;

const SavingTitle = styled.h1`
    font-size: 42px;
`;

const SavingDetails = styled.div`
    h2 {
        font-size: 20px;
    }
    p {
        font-size: 16px;
    }
`;

class Savings extends Component {    
    render() {
        return(
            <SavingContainer>
                <SavingTitle>
                    Savings
                </SavingTitle>
                <SavingDetails>
                    <h2>
                        Daily
                    </h2>
                    <p>
                        {
                            this.props.calculationResult.calculationResult ? `${this.props.calculationResult.calculationResult.savingsDay} ${this.props.chosenCurrency}` : `0 ${this.props.chosenCurrency}`
                        }
                    </p>
                    <h2>
                        Monthy
                    </h2>
                    <p>
                        {
                            this.props.calculationResult.calculationResult ? `${this.props.calculationResult.calculationResult.savingsMonthly} ${this.props.chosenCurrency}` : `0 ${this.props.chosenCurrency}`
                        }
                    </p>
                    <h2>
                        Anually
                    </h2>
                    <p>
                        {
                            this.props.calculationResult.calculationResult ? `${this.props.calculationResult.calculationResult.savingsYear} ${this.props.chosenCurrency}` : `0 ${this.props.chosenCurrency}`
                        }
                    </p>
                </SavingDetails>
            </SavingContainer>
        )
    }
}

const mapStateToProps = state => ({    
    calculationResult: state.calculationResult,
    chosenCurrency: state.chosenCurrency.selectedCurrency
})

export default connect(mapStateToProps, null)(Savings);