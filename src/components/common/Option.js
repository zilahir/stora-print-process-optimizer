import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { setOption } from './../../store/actions/setOption'
import { setResult } from './../../store/actions/setResult';
import { chartLine } from './../../store/actions/chartLine';
import Calculator from "./../Calculator";
import ExcelFile from "./../../xlsx/print-process-optimizer_v2.xlsx";
import { timingSafeEqual } from 'crypto';

const OptionTitle = styled.h1`
    font-size: 22px;
    text-transform: capitalize;
`;

const OptionContainer = styled.div``;

const ButtonContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(5, 50px);
    grid-auto-flow: row; 
    p {
        color: #fff;
        padding: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 30px;
        margin-bottom: 0;
        margin-top: 5px;
        &:hover {
            cursor: pointer;
        }
        border-radius: 10px;
    }
    grid-gap: 10px;
    text-align: center;    
    row-gap: 0;
`;

class Option extends Component {   
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);

        let Writers = {
            pagination: null, 
            reels: null, 
            breaksBefore: null, 
            breaksAfter: null, 
            daysMonitoring: null, 
            weeksMonitoring: null
        }

        let Observers = {            
            costLong: null,
            cumulativeMonthlySavings: null, 
            savingsPer100Reels: null, 
            savingsDay: null, 
            savingsYear: null, 
            savingsMonthly: null, 
            bepMonthLongCalculation: null
        }

        this.calc = new Calculator(ExcelFile, "Sheet1");
        this.calc.calculationStart(Writers, Observers);

        this.state = {
            pagination: null, 
            reels: null, 
            breaksBefore: null, 
            breaksAfter: null    
        }

    }     

    handleClick(item, title) {                        
        this.props.onSetOption(title, item);        
        let Writers = {
            ...this.props.selectedOptions.selectedOptions,
            [title]: item
        }        

        this.setState({
            ...this.state, 
            [title]: item
        })

        this.calc.writers = {...Writers}
        this.calc.execute();        
        
        let currentObservers = this.calc.observers;                
        this.props.onsetResult(currentObservers);        
    }

    render() {                        
        let thisOption = this.props.input
        return (
            <OptionContainer>                
                <OptionTitle>
                    {this.props.title}
                </OptionTitle>
                <ButtonContainer>
                    {
                     this.props.options.map((item, index) => (                         
                         this.state[thisOption] === item ?
                         <p onClick={() => this.handleClick(item, this.props.input)} key={index} style={{backgroundColor: `${this.props.color}`}}>
                             {item}
                         </p>
                         : <p onClick={() => this.handleClick(item, this.props.input)} key={index} style={{backgroundColor: `${this.props.color}`, opacity: `.8`}}>
                             {item}
                         </p>
                     ))
                    }
                </ButtonContainer>
            </OptionContainer>
        )
    }
}

const mapStateToProps = state => ({
    selectedOptions: state.selectedOptions,
    calculationResult: state.calculationResult,
    chartValues: state.chartValues,
    chosenCurrency: state.chosenCurrency.selectedCurrency
})

const mapDispatchToProps = dispatch => ({
    onSetOption: (selectedOptions, selectedValue) => dispatch(setOption(selectedOptions, selectedValue)),
    onsetResult: (calculationResult) => dispatch(setResult(calculationResult)),
    onsetChartLine: (chartLine) => dispatch(chartLine(chartLine))
})

export default connect(mapStateToProps, mapDispatchToProps)(Option);