export const data = {
    labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
    datasets: [
    {
        label: 'what',        
        data: [],
        fill: false,
        lineTension: 0.3,
        backgroundColor: 'transparent',
        borderColor: '#4079ba',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#4079ba',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: '#4079ba',
        pointHoverBorderColor: '#4079ba',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        steppedLine: false
    }
    ]
};