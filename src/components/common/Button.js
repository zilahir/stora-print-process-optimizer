import React from 'react';
import styled from 'styled-components';

const Btn = styled.p`
    background-color: #898b8d;
    padding: 10px;
    &:hover {
        cursor: pointer;
    }
    color: #fff;
    width: 200px;
    text-align: center;
`;

const Button  = ( (props) => {    
    let marginRight = props.color === 'blue' ? '20px' : null;
    let maxWidth = props.color === 'blue' ? '100px' : null;
    let marginTop = props.color === 'blue' ? '50px' : null;    
    let borderColor = props.isActive || props.color === 'blue' ? '#0098d8' : null;    
    let backgroundColor = props.isActive && props.color === 'blue' ? '#0098d8' :  props.color === 'blue' && !props.isActive ? '#fff' : null
    let fontColor = !props.isActive && props.color === 'blue' ? '#0098d8' : '#fff';
    return (
        <Btn style={{color: `${fontColor}`, border: `1px solid ${borderColor}`, marginTop: `${marginTop}`, backgroundColor: `${backgroundColor}`, marginRight: `${marginRight}`, maxWidth: `${maxWidth}`}} onClick={props.onClick}>
            {props.btnText}
        </Btn>
    )
});

export default Button;