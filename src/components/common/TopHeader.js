import React, { Component } from 'react';
import styled from 'styled-components';
import StoraLogo from './../assets/logo.jpg';
import { Link } from 'react-router-dom';

const TopHeaderContainer = styled.div`
    width: 100%;
    height: 130px;
    background-color: #fff;
    box-shadow: 0 0 20px 10px rgba(0, 0, 0, 0.1);
    display: flex;
    align-items: center;    
`;

const LogoContainer = styled.img`
    margin: 0 30px;
    width: 100px;
`;

const MenuContainer = styled.ul`
    display: flex;
    list-style-type: none;
`;

const MenuItem = styled.li`
    a {
        text-decoration: none;
        color: #444;
        font-weight: bold;
        font-size: 22px;        
    }
    padding: 0 20px;
    &:after {
            content: ' ⟶ ';     
            position: relative;
            left: 20px;
        }        
        &:last-of-type{
            &:after {
                content: unset;
            }
        }        
`;

class TopHeader extends Component {
    render() {
        return (
            <TopHeaderContainer>
                <div style={{}}>
                    <LogoContainer src={StoraLogo} />
                </div>
                <div style={{display: `flex`, justifyContent: `center`, margin: `0 auto`, left: `0`, position: `absolute`, right: `0`}}>
                    <MenuContainer>
                        <MenuItem>
                            <Link to="/">
                                Introduction
                            </Link>
                        </MenuItem>
                        <MenuItem>
                            <Link style={{color: `#01a2de`, }} to="/calculator">
                                Setup and savings
                            </Link>
                        </MenuItem>
                        <MenuItem>
                            <Link to="/costs">
                                Costs
                            </Link>
                        </MenuItem>
                    </MenuContainer>
                </div>
            </TopHeaderContainer>
        )
    }
}

export default TopHeader;
